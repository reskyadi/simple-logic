<?php

    $num = 10;

    for ($x=0; $x < $num; $x++) { 
        for ($y=0; $y < $num; $y++) {
            $a = $num-$x-1;
            if ($y >= $a) {
                echo "*";
            }else{
                echo "_";
            }
        }

        for ($y=$num; $y > 0; $y--) { 
            $a = $x;
            if ($a >= $y) {
                echo "*";
            }
        }
        echo "<br>";
    }

    for ($x=$num; $x > 0; $x--) { 
        for ($y=$num; $y > 0; $y--) {
            $a = $x-1;
            if ($y <= $a) {
                echo "*";
            }else{
                if ($x != 1) {
                    echo "_";
                }
            }
        }

        for ($y=$num; $y > 0; $y--) { 
            $a = $x-2;
            if ($a >= $y) {
                echo "*";
            }
        }
        echo "<br>";
    }
    
    // Simple echo data with condition
    $x = $num/2;
    for ($i=0; $i < $num; $i++) { 
        if ($x == $i) {
            echo "Data";
        }
        elseif ($i == $num-1) {
            echo "Data Keluar";
        }
        else {
            echo "-----";
        }
        echo "<br>";
    }

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Text</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
</head>
<body>
    <form action="#" type="get">
        <input type="text" pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}" placeholder="Email Regex" required name="email">
        <input type="submit">
    </form>
</body>
</html>